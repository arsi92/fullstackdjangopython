# INHERITANCE
class Animal():

    def __init__(self):
        print("Animal created")

    def whoAmI(self):
        print("Animal")

    def eat(self):
        print("Eating")


class Dog(Animal):

    def __init__(self):
        Animal.__init__(self)
        print("Dog created")

    def bark(self):
        print("WOOOF!")

    def eat(self):
        print("Dog eating")


# mya = Animal()
# mya.whoAmI()
#
# mydog = Dog()
# mydog.whoAmI()
# mydog.bark()
# mydog.eat()


# SPECIAL METHODS

class Book():

    def __init__(self, title, author, pages):
        self.title = title
        self.author = author
        self.pages = pages

    def __str__(self):
        return "Title: {}, Author: {}, Pages: {}".format(self.title, self.author, self.pages)

    def __len__(self):
        return self.pages

    def __del__(self):
        print("Book is destroyed!")

book1 = Book("Title", "AS", 233)
print(book1)
print(len(book1))

mylist = [1,2,3]
#print(mylist)
