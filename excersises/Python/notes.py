name = 'global'

def greet():
    name = "local greet var"

    def hello():
        print("hello "+name)

    hello()


print(name)
greet()

# don't use global keyword!!!
