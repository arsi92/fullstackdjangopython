"""mylisthere = [1,2,3]

try:
    print(mylist)

except NameError:
    print("Wrong list name")

else:
    print("No exceptions")

finally:
    pass"""

try:
    f = open('text.txt', 'r')
    f.write("New line of text!")

except IOError:
    print("ERROR! COULD NOT FIND FILE OR READ DATA!")

except:
    print("ERROR! Some exception...")

else:
    print("SUCCESS!")
    f.close()

finally:
    print("Finally block...")

print("Hello world!")
