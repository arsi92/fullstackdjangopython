class Dog():

    def __init__(self, breed, name):
        self.breed = breed
        self.name = name
        pass

    # Class object attribute
    species = "mammal"


mydog = Dog(breed = "Lab", name = "Bear")
otherDog = Dog("Husky", "Byku")
print(type(mydog))
print(mydog.breed)
print(otherDog.breed)
print(mydog.species)
