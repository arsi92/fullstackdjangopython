import re

patterns = ['term1', 'term2']

text = 'This is a string with term1, not the other!'

for pattern in patterns:
    print("I'm searching for: " + pattern)

    if re.search(pattern, text):
        print("MATCH!")
    else:
        print("NO MATCH!\n")

#print("")
#match = re.search('term1', text)
#print(match)
#print(match.start())
#print(type(match.start()))


#split_term = '@'
#email = 'artur@ardigen.com'
#print(re.split(split_term, email))


#print(re.findall('match', 'test phrase match in the middle'))


def multi_re_find(patterns, phrase):

    for pat in patterns:
        print("Searching for: {} pattern...".format(pat))
        print(re.findall(pat, phrase))
        print('\n')

#test_phrase = 'sdsd..sssdddd...sdsdsdsd..sdsdss..sdsdsdsdsds'
test_phrase = 'This is a string! But is has punctuation. 121212 How # we can remove it?'

#test_pattern = ['[^!?.]+'] # works like multiple split call
#test_pattern = ['[a-z]+']
#test_pattern = ['[A-Z]+']
#test_pattern = ['[1-9]+']
#test_pattern = [r'\d+'] # digits
#test_pattern = [r'\D+']  # no digits
#test_pattern = [r'\s+']  # whitespaces
#test_pattern = [r'\S+']  # no whitespaces
test_pattern = [r'\w+']  # metacharacters numbers and letters
#test_pattern = [r'\W+']  # no metacharacters like punctuation or #

multi_re_find(test_pattern, test_phrase)

# * - reapeted 0 or more times
# + - 1 or more times
# ? - 0 or 1 time
# {n} - n times
# {n,m} - n or m times

#   ^
#
#
#
#
