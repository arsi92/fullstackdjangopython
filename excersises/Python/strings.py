mystring = 'abcdefg'
print(mystring[2])

#slicing
print(mystring[2:])

# up to but not including
print(mystring[:2])

print(mystring[2:5])

#all string
print(mystring[::])

#skipping co drugi
print(mystring[::2])

x = mystring.upper()
print(x)

x = mystring.lower()
print(x)

x = mystring.capitalize()
print(x)

mystring = "Hello World !"
x = mystring.split()
print(x)

mystring = "Hello World !"
x = mystring.split('o')
print(x)

#formatting
x = "insert some text here: {} and here: {}".format("myk", "pstryk")
print(x)
