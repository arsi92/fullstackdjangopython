// PART 4 ARRAY EXERCISE
// This is  a .js file with commented hints, its optional to use this.

// Create Empty Student Roster Array
// This has been done for you!
var roster = []

// Create the functions for the tasks

// ADD A NEW STUDENT

// Create a function called addNew that takes in a name
// and uses .push to add a new student to the array

function addNew(name){
  roster.push(name);
}

// REMOVE STUDENT

// Create a function called remove that takes in a name
// Finds its index location, then removes that name from the roster

function remove(name){
  var index = roster.indexOf(name);
  if (index > -1){
    roster.splice(index, 1);
  }
}

// HINT: http://stackoverflow.com/questions/5767325/how-to-remove-a-particular-element-from-an-array-in-javascript
//

// DISPLAY ROSTER
// Create a function called display that prints out the roster to the console.

function displayRoster(){
  console.log(roster);
}

// Start by asking if they want to use the web app

var useWebApp = prompt("Do you want to use this web app? y/n");

if (useWebApp == "y"){
  while(true){
    var choice = prompt("Please choose the action what you want to do: add, display, remove, quit")
    if (choice == "add"){
      var nameToAdd = prompt("Please enter the name to add to the roster: ")
      addNew(nameToAdd);
    }
    else if (choice == "display") {
      displayRoster();
    }
    else if (choice == "remove") {
      var nameToRemove = prompt("Please enter the name to remove from the roster: ")
      remove(nameToRemove);
    }
    else if (choice == "quit") {
        break;
    }
  }
}

else {
  console.log("This web app is doesn't used.");
  alert("This web app doesn't used.");
}

// Now create a while loop that keeps asking for an action (add,remove, display or quit)
// Use if and else if statements to execute the correct function for each command.
