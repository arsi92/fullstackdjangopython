from django.shortcuts import render
from django.core.urlresolvers import reverse_lazy
from django.views.generic import (View, TemplateView, ListView, DetailView,
                                  CreateView, UpdateView, DeleteView)
from django.http import HttpResponse
from . import models
# Create your views here.

class IndexView(TemplateView):
    template_name = 'basic_app/index.html'

class SchoolListView(ListView):
    context_object_name = 'schools'
    model = models.School
    # school_list - it returns :D django for us

class SchoolDetailView(DetailView):
    context_object_name = 'school_detail'
    model = models.School
    template_name = 'basic_app/school_detail.html'
    # returns school - that's all

class SchoolCreateView(CreateView):
    fields = ('name', 'principal', 'location')
    model = models.School

class SchoolUpdateView(UpdateView):
    fields = ('name', 'principal')#probably updating of the location is not important
    model = models.School

class SchoolDeleteView(DeleteView):
    model = models.School
    success_url = reverse_lazy("basic_app:list")

#def index(request):
#    return render(request, 'basic_app/index.html')

#class CBView(View):
#    def get(request, self):
#        return HttpResponse("<h1>Class Based View example!!!</h1>")
#
#class IndexView(TemplateView):
#    template_name = 'basic_app/index.html'
#
#    def get_context_data(self, **kwargs):
#        context = super().get_context_data(**kwargs)
#        context['injectme'] = 'BASIC INJECTION!'
#        return context
#
# *args - one,two or how many you want argments - will be a tuple!!! single asterisk
# **kwargs - key word args!!! like dictionary!!! double asteriks
